#define _XOPEN_SOURCE 600 /* For M_PI and fmin. */
#include <float.h>
#include <math.h>
#include <memory.h>

#include "log.h"
#include "track_segment.h"
#include "vehicle.h"

size_t track_segment_parse(struct track_segment *this, cJSON *node)
{
	double length = 0;
	double radius = 0;
	double angle = 0;
	size_t has_switch = 0;
	cJSON *length_node, *radius_node, *angle_node, *switch_node;

	if (node == NULL) {
		log_error("track_segment_parse(this, node): node is NULL.\n");
		return 0;
	}

	length_node = cJSON_GetObjectItem(node, "length");
	if (length_node) {
		length = length_node->valuedouble;
	}

	radius_node = cJSON_GetObjectItem(node, "radius");
	if (radius_node) {
		radius = radius_node->valuedouble;
	}

	angle_node = cJSON_GetObjectItem(node, "angle");
	if (angle_node) {
		angle = angle_node->valuedouble;
		/* value in degrees, convert to radians */
		angle = angle * M_PI/180.0;
	}

	switch_node = cJSON_GetObjectItem(node, "switch");
	if (switch_node && switch_node->type == cJSON_True) {
		has_switch = 1;
	}

	return track_segment_construct(this, length, radius, angle, has_switch);
}

size_t track_segment_construct(struct track_segment *this, double length,
                               double radius, double angle, 
                               size_t has_lane_switch)
{
	if (!this || length < 0 || radius < 0 || (length > 0 && radius > 0))
		return 0;

	memset(this, 0, sizeof(struct track_segment));
	this->has_lane_switch = !!has_lane_switch; /* Force 1 or 0. */

	if (length > 0) {
		this->length = length;
		this->radiuz = 0;
		this->angle = 0;
	} else if (radius > 0) {
		this->radiuz = radius;

		/* Force angle between -PI..PI. */
		this->angle = fmod(angle, 2 * M_PI);
		this->angle -= (this->angle > M_PI) * 2 * M_PI;

		this->length = fabs(this->angle) * radius;
	}

	return 1;
}

double track_segment_get_lane_length(const struct track_segment *this,
		double lane_start_offset, double lane_end_offset)
{
	if (!this)
		return 0.0;

	if (!this->has_lane_switch)
		lane_end_offset = lane_start_offset;

	if (this->angle == 0) {
		double dx = this->length;
		double dy = lane_end_offset - lane_start_offset;

		return sqrt(dx * dx + dy * dy);
	}


	/* Exact solution to bending lane's length would involve solving
	 * a function in polar coordinates analytically:
	 *
	 * query : d/dt q * (t / a) + r * (1 - (t / a))
	 * result: (q - r)/a
	 * query : integrate sqrt((q * (t / a) + r * (1 - (t / a)))^2 +
	 *	((q - r)/a)^2) dt from t = 0 to a
	 * result: Something with 14x sqrt() and 4x log().
	 *
	 * Solution: use mean of length of both lanes (some 1 % too short).
	 */
	return fabs(this->angle) * (this->radiuz - 0.5 * (
		track_segment_get_radius(this, lane_start_offset) +
		track_segment_get_radius(this, lane_end_offset)));
}

double track_segment_get_max_velocity(const struct track_segment *this,
                                      const struct vehicle *vehicle,
                                      unsigned int similar_bend_count,
                                      double lane_start_offset,
                                      double lane_end_offset)
{
	double angular_velocity, distance, n;

	if (!this || !vehicle)
		return 0.0;

	angular_velocity = vehicle->velocity;

	if (this->angle != 0)
		angular_velocity /= track_segment_get_radius(
			this, lane_start_offset);

	/* prints: drift(-2); drift(-1); drift(0); omega */
	log_print("%f,%f,%f,%f,\n",
		vehicle->drift - vehicle->d_drift - 
		(vehicle->d_drift - vehicle->d_d_drift),
		vehicle->drift - vehicle->d_drift,
		vehicle->drift, angular_velocity);

	/* If straight segment: no limits!
	 * If vehicle at halt: just start moving!  */
	if (this->angle == 0 || vehicle->velocity <= 0)
		return DBL_MAX;

	distance = track_segment_get_lane_length(this,
		lane_start_offset, lane_end_offset) * similar_bend_count -
		vehicle->segment_offset;
	n = vehicle_get_ticks_to_distance(vehicle, distance);

	return DBL_MAX;
}

double track_segment_get_radius(const struct track_segment *this,
                                double lane_offset)
{
	double sign;

	if (!this)
		return 0;

	/* Sign is -1 for positive angle, +1 for negative angles and
	 * 0 for straight track segments. */
	sign = (this->angle < 0) - (this->angle > 0);

	return this->radiuz + sign * lane_offset;
}

double track_segment_get_speed_limit(const struct track_segment *this,
                                      unsigned int similar_bend_count,
                                      double lane_offset)
{
	if (!this)
		return 0.0;

	if (this->angle == 0)
		return DBL_MAX;

	/* Measured maximum speeds:
	 *     finland 6.6
	 *     usa     10
	 *     germany 5
	 *     france  4.4
	 *
	 * Using "similar_bend_count * sin(..." instead of "4 * sin(..." leads
	 * to too fast bend exit speeds. Hard coded 4 seems to work always.
	 *
	 * HACKED return value to min(9, value) to avoid crash in USA.
	 */
	return fmin(9.0, sqrt((4.0/3.0) * 
		track_segment_get_radius(this, lane_offset) /
		(4 * sin(fabs(this->angle)))));
}

