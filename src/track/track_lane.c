#include "track_lane.h"

#include "log.h"
#include <string.h>

void track_lane_parse(struct track_lane *this, cJSON *node)
{
	double distance = 0;
	int index = -1;
	cJSON *distance_node, *index_node;

	if (node == NULL) {
		log_error("track_lane_parse(this, node): node is NULL.\n");
		return;
	}

	distance_node = cJSON_GetObjectItem(node, "distanceFromCenter");
	if (distance_node) {
		distance = distance_node->valuedouble;
	} else {
		log_error("node has no distanceFromCenter node.\n");
	}

	index_node = cJSON_GetObjectItem(node, "index");
	if (index_node) {
		index = index_node->valueint;
	} else {
		log_error("node has no index node.\n");
	}

	track_lane_construct(this, distance, index);
}

void track_lane_construct(struct track_lane *this, double distance, int index)
{
	memset(this, 0, sizeof(struct track_lane));
	this->distance = distance;
	this->index = index;
}
