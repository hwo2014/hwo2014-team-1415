#include <math.h>
#include <memory.h>
#include <stddef.h>
#include <stdlib.h>

#include "cJSON.h"
#include "log.h"
#include "track.h"
#include "track_lane.h"
#include "track_segment.h"
#include "vector.h"
#include "vehicle.h"

/**
 * Calculate the maximum entry velocities for each track segment.
 *
 * @param this	Pointer to the subject track structure.
 * @param car	Pointer to the car whose max speed to determine.
 */
static void track_calculate_speed_limits(struct track *this,
                                         const struct vehicle *car);

void track_add_lane(struct track *this, const struct track_lane *lane)
{
	if (this && lane)
		vector_push_back(&this->lanes, lane);
}

void track_add_segment(struct track *this,
                       const struct track_segment *segment)
{
	if (this && segment)
		vector_push_back(&this->segments, segment);
}

static void track_calculate_speed_limits(struct track *this,
                                           const struct vehicle *vehicle)
{
	const struct track_segment *segments;
	const struct track_lane *lanes;
	struct vehicle batmobile;
	double innermost_lane_offset;
	size_t i, last_segments_unified;

	if (!this || this->segments.elem_cnt < 1 || !vehicle)
		return;

	segments = (const struct track_segment *)this->segments.elem;
	lanes = (const struct track_lane *)this->lanes.elem;
	vector_clear(&this->speed_limits);

	innermost_lane_offset = 0;
	for (i = 0 ; i < this->lanes.elem_cnt; ++i) {
		if (lanes[i].distance > innermost_lane_offset)
			innermost_lane_offset = lanes[i].distance;
	}

	batmobile = *vehicle; /* Shallow copy.*/
	batmobile.segment_index = 0;
	batmobile.segment_offset = 0;
	batmobile.velocity = 0;
	batmobile.drift = 0;

	last_segments_unified = 0;
	for (i = 0; i < this->segments.elem_cnt; ++i) {
		/* TODO: Calculate max velocity for each lane! */
		double speed_limit = track_segment_get_speed_limit(
			&segments[i],
			track_get_adjacent_similar_segment_count(this, i),
			innermost_lane_offset);
		size_t j;

		vector_push_back(&this->speed_limits, &speed_limit);
		log_print("Track segment %d got speed limit %e.\n",
			i, speed_limit);

		/* Make sure that the speed limit can be respected i.e.
		 * that the vehicle is able to decelerate to this segment's
		 * speed limit during the previous track segment.
		 */
		one_more_time:
		for (j = i - 1; j < i; --j) { /* NOTE: j is unsigned! */
			double prev_speed_limit =
				((double *)this->speed_limits.elem)[j];
			double segment_length, new_velocity;

			if (prev_speed_limit <= speed_limit)
				break; /* No deceleration necessary! */

			log_print("Track segment %d has CURRENT "
				"speed limit %e.\n", j, prev_speed_limit);

			segment_length = track_segment_get_lane_length(
				&segments[j], 0, 0);
			new_velocity = vehicle_get_initial_velocity(
				&batmobile, speed_limit, segment_length);

			if (prev_speed_limit < new_velocity) {
				log_print("Vehicle (a: %f, b: %f) is able to "
					"decelerate from %e to %e under %f "
					"distance units.\n", 
					batmobile.vel_const_a,
					batmobile.vel_const_b,
					prev_speed_limit, speed_limit,
					segment_length);

				break; /* OK, vehicle can decelerate. */
			}

			/* The previous segment got a new maximum velocity.
			 * It might be that the segment preceding the previous
			 * segment allows too high velocities, so it needs to
			 * be processed similar to this segment.
			 */
			((double *)this->speed_limits.elem)[j] = new_velocity;
			speed_limit = new_velocity;
			log_print("Track segment %d got NEW "
				"speed limit %e.\n", j, new_velocity);
		}

		/* At this point it might be that track's last segments have
		 * no speed limits although they should, because they lead
		 * directly to the first segment. Loop back from the last
		 * segment and ensure they comply to the first segment's
		 * limitations. */
		if (!last_segments_unified && i+1 == this->segments.elem_cnt) {
			last_segments_unified = 1;
			speed_limit = ((double *)this->speed_limits.elem)[0];
			i += 1;
			goto one_more_time;
		}

		if (last_segments_unified)
			break;
	}

}

void track_construct(struct track *this, struct message *game_init)
{
	cJSON *data_node, *race_node, *track_node, *t_name_node, *t_id_node;
	cJSON *pieces_node, *lanes_node, *tmp;
	int pieces_size, lanes_size, i, name_size, id_size;
	struct track_segment *segment;
	struct track_lane *lane;

	memset(this, 0, sizeof(struct track));
	vector_construct(&this->lanes, sizeof(struct track_lane), 8);
	vector_construct(&this->segments, sizeof(struct track_segment), 8);
	vector_construct(&this->speed_limits, sizeof(double), 8);

	data_node = cJSON_GetObjectItem(game_init->json, "data");
	if (data_node == NULL) {
		log_error("message contains no data node.\n");
		return;
	}

	race_node = cJSON_GetObjectItem(data_node, "race");
	if (race_node == NULL) {
		log_error("message contains no race node.\n");
		return;
	}

	track_node = cJSON_GetObjectItem(race_node, "track");
	if (track_node == NULL) {
		log_error("race node contains no track node.\n");
		return;
	}

	t_id_node = cJSON_GetObjectItem(track_node, "id");
	if (t_id_node) {
		id_size = strlen(t_id_node->valuestring) + 1;
		this->id = malloc(id_size);
		memcpy(this->id, t_id_node->valuestring, id_size);
	} else {
		log_print("track has no id.\n");
	}

	t_name_node = cJSON_GetObjectItem(track_node, "name");
	if (t_name_node) {
		name_size = strlen(t_name_node->valuestring) + 1;
		this->name = malloc(name_size);
		memcpy(this->name, t_name_node->valuestring, name_size);
	} else {
		log_print("track has no name.\n");
	}

	pieces_node = cJSON_GetObjectItem(track_node, "pieces");
	if (pieces_node) {
		pieces_size = cJSON_GetArraySize(pieces_node);

		for (i=0; i<pieces_size; ++i) {
			tmp = cJSON_GetArrayItem(pieces_node, i);
			segment = malloc(sizeof(struct track_segment));
			track_segment_parse(segment, tmp);
			track_add_segment(this, segment);
			free(segment);
		}
	} else {
		log_print("track has no pieces (segments).\n");
	}

	lanes_node = cJSON_GetObjectItem(track_node, "lanes");
	if (lanes_node) {
		lanes_size = cJSON_GetArraySize(lanes_node);

		for (i=0; i<lanes_size; ++i) {
			tmp = cJSON_GetArrayItem(lanes_node, i);
			lane = malloc(sizeof(struct track_lane));
			track_lane_parse(lane, tmp);
			track_add_lane(this, lane);
			free(lane);
		}
	} else {
		log_print("track has no lanes.\n");
	}
}

void track_destroy(struct track *this)
{
	vector_destroy(&this->speed_limits);
	vector_destroy(&this->segments);
	vector_destroy(&this->lanes);
	free(this->id);
	free(this->name);

	memset(this, 0, sizeof(struct track));
}

unsigned int track_get_adjacent_similar_segment_count(const struct track *this,
                                                   unsigned int segment_index)
{
	const struct track_segment *segments;
	unsigned int i, count;

	if (!this || segment_index >= this->segments.elem_cnt)
		return 0;

	segments = ((const struct track_segment *)this->segments.elem);
	count = 1;
	i = segment_index + 1;

	while (1 == 1) {
		i = i % this->segments.elem_cnt;

		if (segments[segment_index].angle == segments[i].angle &&
		    track_segment_get_radius(&segments[segment_index], 0) ==
		    track_segment_get_radius(&segments[i], 0)) {
			/* Identical segment: accept and proceed. */
			i++;
			count++;
		} else if (segments[segment_index].angle * 
		           segments[i].angle < 0) {
			/* Bend to opposite direction: accept and proceed. */
			i++;
			count++;
		} else {
			break;
		}
	}

	return count;
}

double track_get_max_vehicle_speed(struct track *this,
                                   const struct vehicle *vehicle)
{
	const struct track_segment *segments;
	const struct track_lane *lanes;
	double velocity_max;
	size_t next_segm_index;

	if (!this || !vehicle ||
	    vehicle->segment_index >= this->segments.elem_cnt ||
	    vehicle->lane_index_start >= this->lanes.elem_cnt)
		return 0.0;

	segments = this->segments.elem;
	lanes = this->lanes.elem;

	velocity_max = track_segment_get_max_velocity(
		&segments[vehicle->segment_index], vehicle,
		track_get_adjacent_similar_segment_count(this,
			vehicle->segment_index),
		lanes[vehicle->lane_index_start].distance,
		lanes[vehicle->lane_index_end].distance);

	if (this->speed_limits.elem_cnt < this->segments.elem_cnt)
		track_calculate_speed_limits(this, vehicle);

	/* The speed limit is either the current speed limit or the
	 * EXIT speed limit i.e. next segment's entry limit. */
	next_segm_index = (1 + vehicle->segment_index) % 
		this->segments.elem_cnt;

	return fmin(((double *)this->speed_limits.elem)[
		next_segm_index], velocity_max);
}

unsigned int track_get_number_of_switches(const struct track *this)
{
	unsigned int i, switches = 0;
	const struct track_segment *segment, *segments;

	if (!this)
		return 0;

	segments = this->segments.elem;

	for (i=0; i<this->segments.elem_cnt; ++i) {
		segment = &segments[i];
		if (segment->has_lane_switch) {
			++switches;
		}
	}

	return switches;
}

double track_get_distance_to_switch(const struct track *this,
                                    const struct vehicle *car)
{
	unsigned int i;
	double accumulated_distance = 0.0;
	const struct track_segment *segment, *segments;

	if (!this || !car)
		return -1.0;

	if (track_get_number_of_switches(this) == 0)
		return -1.0;

	i = car->segment_index;
	segments = this->segments.elem;
	segment = &segments[i];

	if (segment->has_lane_switch)
		return 0.0;

	accumulated_distance = segment->length - car->segment_offset;

	while (1) {
		i = (i+1)%this->segments.elem_cnt;
		segment = &segments[i];

		if (segment->has_lane_switch) {
			return accumulated_distance;
		}

		accumulated_distance += segment->length;
	}
}

double track_get_distance_between_switches(const struct track *this,
                                           const struct vehicle *car,
                                           unsigned int lane_idx)
{
	int first_switch_idx = -1, second_switch_idx = -1, i;
	const struct track_segment *segment, *segments;

	if (!this || !car)
		return -1.0;

	if (track_get_number_of_switches(this) == 0)
		return -1.0;

	i = car->segment_index;
	segments = this->segments.elem;

	while (1) {
		i = (i+1)%this->segments.elem_cnt;
		segment = &segments[i];

		if (segment->has_lane_switch) {
			if (first_switch_idx == -1) {
				first_switch_idx = i;
			} else if (second_switch_idx == -1) {
				second_switch_idx = i;
				break;
			}
		}
	}

	return track_get_distance_between_segments_on_lane(this,
	                                                   first_switch_idx,
	                                                   second_switch_idx,
	                                                   lane_idx);
}

double track_get_distance_between_segments_on_lane(const struct track *this,
                                                   int a, int b,
                                                   unsigned int lane_idx)
{
	const struct track_segment *segment, *segments;
	const struct track_lane *lanes;
	double lane_offset;
	double accumulated_distance = 0;

	if (!this || lane_idx >= this->lanes.elem_cnt)
		return -1.0;

	lanes = this->lanes.elem;
	lane_offset = (&lanes[lane_idx])->distance;

	segments = this->segments.elem;

	do {
		segment = &segments[a];
		if (segment->angle == 0) {
			accumulated_distance += segment->length;
		} else {
			accumulated_distance += fabs(segment->angle) *
			track_segment_get_radius(segment, lane_offset);
		}

		a = (a+1)%this->segments.elem_cnt;
	} while (a != b);

	return accumulated_distance;
}

void track_print(struct track *this)
{
	unsigned int i;
	const struct track_segment *segment;
	const struct track_lane *lane;

	log_print("struct track {\n");
	log_print("  id: %s\n", this->id);
	log_print("  name: %s\n", this->name);

	log_print("  segments [\n");
	for (i=0; i<this->segments.elem_cnt; ++i) {
		const struct track_segment *segments = this->segments.elem;
		segment = &segments[i];
		log_print("    {\n");
		log_print("      length: %f\n", segment->length);
		log_print("      radius: %f\n", segment->radiuz);
		log_print("      angle: %f\n", segment->angle);
		log_print("      has_switch: %u\n", segment->has_lane_switch);
		log_print("    },\n");
	}
	log_print("  ]\n");

	log_print("  lanes [\n");
	for (i=0; i<this->lanes.elem_cnt; ++i) {
		const struct track_lane *lanes = this->lanes.elem;
		lane = &lanes[i];
		log_print("    {\n");
		log_print("      distanceFromCenter: %f\n", lane->distance);
		log_print("      index: %d\n", lane->index);
		log_print("    },\n");
	}
	log_print("  ]\n");

	log_print("}\n");
}
