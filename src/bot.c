#include "bot.h"

#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

#include "ai_switch_lane.h"
#include "log.h"

#define BOT_BUFSIZE 16*1024

void message_print(struct message *this, const char *type)
{
	char *msgType;

	switch (this->type) {
	case BOT_MSG_TYPE_JOIN:           msgType = "join";          break;
	case BOT_MSG_TYPE_THROTTLE:       msgType = "throttle";      break;
	case BOT_MSG_TYPE_PING:           msgType = "ping";          break;
	case BOT_MSG_TYPE_CREATE_RACE:    msgType = "createRace";    break;
	case BOT_MSG_TYPE_JOIN_RACE:      msgType = "joinRace";      break;
	case BOT_MSG_TYPE_YOUR_CAR:       msgType = "yourCar";       break;
	case BOT_MSG_TYPE_GAME_INIT:      msgType = "gameInit";      break;
	case BOT_MSG_TYPE_GAME_START:     msgType = "gameStart";     break;
	case BOT_MSG_TYPE_CAR_POSITIONS:  msgType = "carPositions";  break;
	case BOT_MSG_TYPE_GAME_END:       msgType = "gameEnd";       break;
	case BOT_MSG_TYPE_TOURNAMENT_END: msgType = "tournamentEnd"; break;
	case BOT_MSG_TYPE_CRASH:          msgType = "crash";         break;
	case BOT_MSG_TYPE_SPAWN:          msgType = "spawn";         break;
	case BOT_MSG_TYPE_LAP_FINISHED:   msgType = "lapFinished";   break;
	case BOT_MSG_TYPE_DNF:            msgType = "dnf";           break;
	case BOT_MSG_TYPE_FINISH:         msgType = "finish";        break;
	case BOT_MSG_TYPE_SWITCH_LANE:    msgType = "switchLane";    break;
	case BOT_MSG_TYPE_TURBO_AVAIL:    msgType = "turboAvailable";break;
	case BOT_MSG_TYPE_TURBO_ACTIVATE: msgType = "turbo";         break;
	default:                          msgType = "unknown";       break;
	}

	if (this->type == BOT_MSG_TYPE_THROTTLE) {
		log_print("%s { msgType: %s, data: %.3f }\n", type, msgType,
		          cJSON_GetObjectItem(this->json, "data")->valuedouble);
	} else if (this->type == BOT_MSG_TYPE_SWITCH_LANE) {
		log_print("%s { msgType: %s, data: %s }\n", type, msgType,
		          cJSON_GetObjectItem(this->json, "data")->valuestring);
	} else {
		if (this->game_tick == -1) {
			log_print("%s { msgType: %s }\n", type, msgType);
		} else {
			log_print("%s { msgType: %s, gameTick: %d }\n", type,
			          msgType, this->game_tick);
		}
	}
}

unsigned int message_type(const char *str)
{
	if (strcmp("join", str) == 0) {
		return BOT_MSG_TYPE_JOIN;
	} else if (strcmp("throttle", str) == 0) {
		return BOT_MSG_TYPE_THROTTLE;
	} else if (strcmp("turbo", str) == 0) {
		return BOT_MSG_TYPE_TURBO_ACTIVATE;
	} else if (strcmp("ping", str) == 0) {
		return BOT_MSG_TYPE_PING;
	} else if (strcmp("createRace", str) == 0) {
		return BOT_MSG_TYPE_CREATE_RACE;
	} else if (strcmp("joinRace", str) == 0) {
		return BOT_MSG_TYPE_JOIN_RACE;
	} else if (strcmp("yourCar", str) == 0) {
		return BOT_MSG_TYPE_YOUR_CAR;
	} else if (strcmp("gameInit", str) == 0) {
		return BOT_MSG_TYPE_GAME_INIT;
	} else if (strcmp("gameStart", str) == 0) {
		return BOT_MSG_TYPE_GAME_START;
	} else if (strcmp("carPositions", str) == 0) {
		return BOT_MSG_TYPE_CAR_POSITIONS;
	} else if (strcmp("gameEnd", str) == 0) {
		return BOT_MSG_TYPE_GAME_END;
	} else if (strcmp("tournamentEnd", str) == 0) {
		return BOT_MSG_TYPE_TOURNAMENT_END;
	} else if (strcmp("crash", str) == 0) {
		return BOT_MSG_TYPE_CRASH;
	} else if (strcmp("spawn", str) == 0) {
		return BOT_MSG_TYPE_SPAWN;
	} else if (strcmp("lapFinished", str) == 0) {
		return BOT_MSG_TYPE_LAP_FINISHED;
	} else if (strcmp("dnf", str) == 0) {
		return BOT_MSG_TYPE_DNF;
	} else if (strcmp("finish", str) == 0) {
		return BOT_MSG_TYPE_FINISH;
	} else if (strcmp("switchLane", str) == 0) {
		return BOT_MSG_TYPE_SWITCH_LANE;
	} else if (strcmp("turboAvailable", str) == 0) {
		return BOT_MSG_TYPE_TURBO_AVAIL;
	} else {
		return BOT_ERROR;
	}
}

void message_read(struct message *this, int fd)
{
	/* TODO: hardcoded buffer size, what if server send larget data */
	ssize_t bytes_read = 0;
	char buf[BOT_BUFSIZE] = {0};
	size_t index = 0;

	while (index < BOT_BUFSIZE){
		bytes_read = read(fd, buf + index, 1);
		if (bytes_read < 0){
			this->type = BOT_ERROR;
			log_error("recv failed: %d\n", bytes_read);
			return;
		}
		if (*(buf + index) == '\n'){
			break;
		}
		++index;
	}

	this->json = cJSON_Parse(buf);
	if (this->json == NULL) {
		this->type = BOT_ERROR;
		log_error("malformed JSON(%s): %s\n", cJSON_GetErrorPtr(), buf);
		return;
	}

	message_parse(this, this->json);
}

void message_parse(struct message *this, cJSON *json)
{
	cJSON *game_tick = NULL, *msg_type = NULL;
	msg_type = cJSON_GetObjectItem(json, "msgType");
	if (msg_type == NULL) {
		this->type = BOT_ERROR;
		log_error("No msgType attribute given.\n");
		return;
	}

	this->type = message_type(msg_type->valuestring);

	game_tick = cJSON_GetObjectItem(json, "gameTick");
	if (game_tick != NULL) {
		this->game_tick = game_tick->valueint;
	} else {
		this->game_tick = -1;
	}
}

void message_create_join(struct message *this, const char *name, const char *key)
{
	cJSON *data;

	this->type = BOT_MSG_TYPE_JOIN;
	this->json = cJSON_CreateObject();
	cJSON_AddStringToObject(this->json, "msgType", "join");
	data = cJSON_CreateObject();
	cJSON_AddStringToObject(data, "name", name);
	cJSON_AddStringToObject(data, "key", key);
	cJSON_AddItemToObject(this->json, "data", data);
}

void message_create_adv_join(struct message *this, const char *name, const char *key,
	const char *track, const char *password, const size_t car_count)
{
	cJSON *data, *botId;

	this->type = BOT_MSG_TYPE_JOIN_RACE;
	this->json = cJSON_CreateObject();
	cJSON_AddStringToObject(this->json, "msgType", "joinRace");
	data = cJSON_CreateObject();
	botId = cJSON_CreateObject();
	cJSON_AddItemToObject(data, "botId", botId);
	cJSON_AddStringToObject(botId, "key", key);
	cJSON_AddStringToObject(botId, "name", name);
	if (track){
		cJSON_AddStringToObject(data, "trackName", track);
	}
	if (password){
		cJSON_AddStringToObject(data, "password", password);
	}
	cJSON_AddNumberToObject(data, "carCount", car_count);
	cJSON_AddItemToObject(this->json, "data", data);
}

void message_create_ping(struct message *this)
{
	this->type = BOT_MSG_TYPE_PING;
	this->json = cJSON_CreateObject();
	cJSON_AddStringToObject(this->json, "msgType", "ping");
}

void message_create_throttle(struct message *this, double throttle)
{
	this->type = BOT_MSG_TYPE_THROTTLE;
	this->json = cJSON_CreateObject();
	cJSON_AddStringToObject(this->json, "msgType", "throttle");
	cJSON_AddItemToObject(this->json, "data", cJSON_CreateNumber(throttle));
}

void message_create_turbo(struct message *this)
{
	this->type = BOT_MSG_TYPE_TURBO_ACTIVATE;
	this->json = cJSON_CreateObject();
	cJSON_AddStringToObject(this->json, "msgType", "turbo");
	cJSON_AddItemToObject(this->json, "data",
		cJSON_CreateString("turbo"));
}

void message_create_switch_lane(struct message *this,
                                struct switch_lane *switch_lane)
{
	this->type = BOT_MSG_TYPE_SWITCH_LANE;
	this->json = cJSON_CreateObject();
	cJSON_AddStringToObject(this->json, "msgType", "switchLane");
	if (switch_lane->chosen_lane == AI_SWITCH_LANE_LEFT) {
		cJSON_AddStringToObject(this->json, "data", "Left");
	} else if (switch_lane->chosen_lane == AI_SWITCH_LANE_RIGHT) {
		cJSON_AddStringToObject(this->json, "data", "Right");
	} else {
		log_error("Something seems to be wrong with switch_lanes.\n");
	}
}

void message_free(struct message *this)
{
	if (this && this->json) {
		cJSON_Delete(this->json);
		this->json = NULL;
	}
}

void message_send(struct message *this, int fd)
{
	char nl = '\n';
	char *msg_str;

	msg_str = cJSON_PrintUnformatted(this->json);

	write(fd, msg_str, strlen(msg_str));
	write(fd, &nl, 1);

	free(msg_str);
}
