#include "ai_turbo.h"

#include "track.h"
#include "track_segment.h"
#include "vector.h"
#include "vehicle.h"

size_t ai_assess_turbo(const struct vehicle *our_vehicle,
                     const struct track *track,
                     const struct vector *vehicles)
{
	double speed_limit, final_velocity;
	unsigned int is_last_lap, is_straight_track, sim_segm_count;

	if (!our_vehicle || !track || !vehicles ||
	    !our_vehicle->has_turbo || our_vehicle->turbo_factor < 1 ||
	    our_vehicle->turbo_duration < 1)
		return 0;

	speed_limit = ((double *)track->speed_limits.elem)[
		our_vehicle->segment_index];
	is_last_lap = our_vehicle->lap_count == 2;
	is_straight_track = ((const struct track_segment *)
		track->segments.elem)[our_vehicle->segment_index].angle == 0;
	sim_segm_count = track_get_adjacent_similar_segment_count(track,
		our_vehicle->segment_index);

	/* Calculate the final velocity if we use turbo now. It cannot be
	 * expected that vehicle uses full throttle all the time nor is it
	 * smart to use turbo and break with all force, so some heuristical
	 * (guessed) value is used for throttle during turbo.
	 */
	final_velocity = our_vehicle->turbo_factor * 
		vehicle_get_velocity_at_tick(our_vehicle, 
		(size_t)((int)our_vehicle->turbo_duration), 0.5);

	/* Allow turbo on straight segments that are either long enough to
	 * not cause too much drift in the next curve OR that are the LAST
	 * straight before the finishing line, */
	return (final_velocity < speed_limit && is_straight_track) ||
		(is_last_lap && our_vehicle->segment_index + sim_segm_count >=
			track->segments.elem_cnt && is_straight_track);
}
