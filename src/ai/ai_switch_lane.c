#include "ai_switch_lane.h"

#include "track.h"
#include "track_lane.h"
#include "vector.h"
#include "vehicle.h"

#include <stdlib.h>
#include <string.h>

void ai_switch_lane_construct(struct switch_lane *this)
{
	memset(this, 0, sizeof(struct switch_lane));
	this->state = AI_SWITCH_LANE_STATE_UNDECIDED;
}

void ai_assess_lanes(struct switch_lane *state, struct track *track,
                     struct vector *vehicles, struct vehicle *our_vehicle)
{
	int cur_lane_idx;
	int chosen_lane_idx = -1;
	unsigned int i, is_blocked_at_chosen_lane;
	double distance, min_distance, tmp_distance;
	const struct track_lane *lane, *lanes;

	/* Are we still switching */
	if (state->state == AI_SWITCH_LANE_STATE_DECIDED &&
	    state->decision_segment_idx == (int)our_vehicle->segment_index)
		return;

	state->state = AI_SWITCH_LANE_STATE_UNDECIDED;
	state->decision_segment_idx = -1;
	state->sent = 0;

	distance = track_get_distance_to_switch(track, our_vehicle);
	if (distance <= 0)
		return;

	/* when is the next switch? if too far don't decide yet */
	if (distance > AI_SWITCH_LANE_NOT_BEFORE)
		return;

	state->state = AI_SWITCH_LANE_STATE_DECIDED;
	state->decision_segment_idx = our_vehicle->segment_index;

	cur_lane_idx = our_vehicle->lane_index_start;
	lanes = track->lanes.elem;

	min_distance = 10000000; /* yeah... */

	/* what would be the length to the next switch on given lane */
	for (i = 0; i < track->lanes.elem_cnt; ++i) {
		lane = &lanes[i];
		if (abs(lane->index - cur_lane_idx) > 1) {
			/* can't change there */
			continue;
		}

		tmp_distance = track_get_distance_between_switches(track,
		                                                   our_vehicle,
		                                                   lane->index);

		if (tmp_distance < min_distance) {
			min_distance = tmp_distance;
			chosen_lane_idx = lane->index;
		} else if (tmp_distance == min_distance &&
		           lane->index == cur_lane_idx)
		{
			/* lanes are both as short, then prefer current. */
			min_distance = tmp_distance;
			chosen_lane_idx = lane->index;
		}
	}

	is_blocked_at_chosen_lane = 0;

	/* No lane was chosen. Mark the current lane as chosen lane in order
	 * to correctly perform blocking test. */
	if (chosen_lane_idx == -1)
		chosen_lane_idx = our_vehicle->lane_index_start;

	for (i = 0; !is_blocked_at_chosen_lane && i < 3; ++i) {
		const struct vehicle *vehicles_array = 
			((const struct vehicle *)vehicles->elem);
		const unsigned int segm_index =
			(state->decision_segment_idx + i) %
			track->segments.elem_cnt;
		unsigned int j;

		for (j = 0; j < vehicles->elem_cnt; ++j) {
			/*log_print("%s: %d vs. %d vs. %d, %d vs. %d, %f < %f\n",
				our_vehicle->name,
				(int)vehicles_array[j].lane_index_start,
				(int)vehicles_array[j].lane_index_end,
				chosen_lane_idx,
				vehicles_array[j].segment_index,
				segm_index,
				vehicles_array[j].velocity,
				our_vehicle->velocity);
			*/	
			if ((int)vehicles_array[j].lane_index_start ==
			    chosen_lane_idx && chosen_lane_idx ==
			    (int)vehicles_array[j].lane_index_end &&
			    vehicles_array[j].segment_index == segm_index &&
			    vehicles_array[j].velocity <
			    our_vehicle->velocity * 0.9) {
				is_blocked_at_chosen_lane = 1;
				break;
			}
		}
	}

	if (chosen_lane_idx != -1) {
		if (is_blocked_at_chosen_lane && 
		    track->lanes.elem_cnt > 1) {
			const int old_lane = chosen_lane_idx;

			/* Just pick a random lane to switch to. */
			while (chosen_lane_idx == old_lane)
				chosen_lane_idx = rand() % 
					track->lanes.elem_cnt;
		}

		if (cur_lane_idx == chosen_lane_idx) {
			state->chosen_lane = AI_SWITCH_LANE_NO;
			state->sent = 1;
		} else if (chosen_lane_idx < cur_lane_idx) {
			state->chosen_lane = AI_SWITCH_LANE_LEFT;
		} else {
			state->chosen_lane = AI_SWITCH_LANE_RIGHT;
		}
	} else {
		state->chosen_lane = AI_SWITCH_LANE_NO;
		state->sent = 1;
	}
}
