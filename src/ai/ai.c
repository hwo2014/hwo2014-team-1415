#include "ai.h"
#include "log.h"
#include "track.h"
#include "track_lane.h"
#include "track_segment.h"
#include "vector.h"
#include "vehicle.h"

#include <math.h>
#include <string.h>

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

double ai_get_throttle(const int ai_state, 
	struct track *track, 
	const struct vector *vehicle_vct,
	struct vehicle *our_vehicle)
{
	size_t i = ai_state; /* Remove -pedantic warning. */

	if (0 == strcmp("Porridge D", our_vehicle->name)) {
		return ai_get_predictive_throttle(track, our_vehicle, 4);
	} else if (0 == strcmp("SLOWASS", our_vehicle->name)) {
		return 0.20;
	}
	else{
		double throttle = our_vehicle->velocity < 
			track_get_max_vehicle_speed(track, our_vehicle);
		our_vehicle->throttle = throttle;

		return throttle;
	}
}

double ai_get_predictive_throttle(const struct track *track,
                                  const struct vehicle *our_vehicle,
                                  unsigned int n)
{
	const struct track_segment *segments;
	const struct track_lane *lanes;
	unsigned int i, cur;
	double weight, radius, angle, lane_offset;
	double throttle = 1.0, reduction;

	segments = track->segments.elem;
	lanes = track->lanes.elem;
	lane_offset = (&lanes[our_vehicle->lane_index_start])->distance;
	cur = our_vehicle->segment_index;

	for (i=1; i<=n; ++i) {
		const struct track_segment *segment = &segments[(cur+i)%track->segments.elem_cnt];
		weight = 1.0 - ((double) i)/(n+1.0);

		if (segment->angle != 0) {
			angle = segment->angle;
			radius = track_segment_get_radius(segment, lane_offset);
			reduction = weight * (fabs(angle)/M_PI) * radius/segment->radiuz;
			throttle -= reduction;
		}
	}

	if (throttle < our_vehicle->velocity * 0.1) {
		throttle *= 0.75; /* more aggresive breaking. */
	}

	return MIN(1.0, MAX(0.0, throttle));
}
