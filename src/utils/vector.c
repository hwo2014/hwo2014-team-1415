#include <stdlib.h>
#include <string.h>

#include "vector.h"

void vector_clear(struct vector *this)
{
	this->elem_cnt = 0;
}

void vector_construct(struct vector *this, unsigned int size,
                      unsigned int capacity)
{
	this->elem = malloc(size * capacity);
	this->elem_size = size;
	this->elem_max = capacity;
	this->elem_cnt = 0;
}

void vector_destroy(struct vector *this)
{
	free(this->elem);
	this->elem = 0;
}

void vector_push_back(struct vector *this, const void *elem)
{
	if (this->elem_cnt >= this->elem_max) {
		void *tmp = this->elem;

		this->elem_max *= 2;
		tmp = realloc(tmp, this->elem_max * this->elem_size);

		/* TODO: handle NULL pointer i.e. out-of-memory situation. */

		this->elem = tmp;
	}

	memcpy((char *)this->elem + this->elem_cnt * this->elem_size,
		elem, this->elem_size);
	++this->elem_cnt;
}

void vector_pop_back(struct vector *this)
{
	--this->elem_cnt;
}

unsigned int vector_remove(struct vector *this, unsigned int i)
{
	if (i >= this->elem_cnt)
		return 0;

	--this->elem_cnt;
	memcpy((char *)this->elem + i              * this->elem_size,
	       (char *)this->elem + this->elem_cnt * this->elem_size,
	       this->elem_size);

	return 1;
}

