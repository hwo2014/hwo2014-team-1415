#define _XOPEN_SOURCE 600 /* For M_PI and pow(). */
#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "log.h"
#include "track.h"
#include "vehicle.h"
#include <stdio.h>

void parse_id_node(cJSON *node, char **name, char **color)
{
	cJSON *name_node, *color_node;
	size_t name_size, color_size;

	name_node = cJSON_GetObjectItem(node, "name");
	if (name_node) {
		name_size = strlen(name_node->valuestring) + 1;
		*name = malloc(name_size);
		memcpy(*name, name_node->valuestring, name_size);
	} else {
		log_error("id node has no name node.\n");
	}

	color_node = cJSON_GetObjectItem(node, "color");
	if (color_node) {
		color_size = strlen(color_node->valuestring) + 1;
		*color = malloc(color_size);
		memcpy(*color, color_node->valuestring, color_size);
	} else {
		log_error("id node has no color node.\n");
	}
}

void parse_your_car_node(struct message *msg, char **name, char **color)
{
	cJSON *data_node = cJSON_GetObjectItem(msg->json, "data");
	if (data_node == NULL) {
		log_error("json contains no data node.\n");
		return;
	}

	parse_id_node(data_node, name, color);
}

size_t vehicle_construct(struct vehicle *this, const char *name,
                         const char *color, double length, double width,
                         double guide_flag_pos)
{
	size_t name_len, color_len;

	if (!this || !name || !color)
		return 0;

	memset(this, 0, sizeof(struct vehicle));

	name_len = strlen(name) + 1;
	this->name = malloc(name_len);
	memcpy(this->name, name, name_len);

	color_len = strlen(color) + 1;
	this->color = malloc(name_len);
	memcpy(this->color, color, color_len);

	this->length = length;
	this->width = width;
	this->guide_flag_offset = guide_flag_pos;

	this->vel_const_a = 0.98;
	this->vel_const_b = 0.2;
	this->drift_max = M_PI / 3.14; /* ~60 degrees. */

	this->turbo_factor = 1.0;
	this->turbo_duration = 0.0;
	this->has_turbo = 0;

	return 1;
}

void vehicle_destroy(struct vehicle *this)
{
	free(this->name);
	free(this->color);

	memset(this, 0, sizeof(struct vehicle));
}

void vehicle_enable_turbo(struct vehicle *this,
                          double turbo_factor, double duration)
{
	if (this) {
		this->has_turbo = 1;
		this->turbo_factor = turbo_factor;
		this->turbo_duration = duration;
	}
}

double vehicle_get_initial_velocity(const struct vehicle *this,
		double target_velocity, double max_distance)
{
	double a, b, L, n, V, X;

	if (!this)
		return 0.0;

	a = this->vel_const_a;
	b = this->vel_const_b;
	L = max_distance;
	V = target_velocity;
	X = (a/(a - 1))/(a/(a - 1) - L/V);

	/* Avoid negative log and division by zero. */
	if (a <= 0 || fabs(a - 1) < 0.0000001 || X <= 0)
		return 0;

	n = log(X)/log(a);
	X = pow(a, floor(n));

	if (fabs(X) < 0.0000001)
		return 0.0;

	return V/X;

}

double vehicle_get_ticks_to_distance(const struct vehicle *this,
                                     double distance)
{
/*	double a, b, n, T;*/

	if (!this || this->velocity <= 0 || distance <= 0)
		return 0.0;

	return distance / this->velocity;

	#if 0
	a = this->vel_const_a;
	b = this->vel_const_b;
	T = this->velocity * (1 - a) / b;

	/* Avoid negative log and division by zero. */
	if (a <= 0 || fabs(a - 1) < 0.0000001)
		return 0;

	n = 1;
	while (n < 1000) { /* Prevent infinite loop. */
		double X = ((pow(a, n+1) - 1)/(a - 1) - 1);

		if (distance < X * b * T / (a - 1) + this->velocity * X)
			break;

		n += 1.0;
	}

	return n - 1;
	#endif
}

double vehicle_get_velocity_at_tick(const struct vehicle *this,
                                    size_t tick_count, double throttle)
{
	double a, b, a_pow_n;

	/* Avoid null pointer and division by zero. */
	if (!this || fabs(this->vel_const_a - 1) < 0.0000001)
		return 0.0;

	a = this->vel_const_a;
	b = this->vel_const_b;
	a_pow_n = pow(this->vel_const_a, tick_count);

	return a_pow_n * this->velocity + b * throttle * (a_pow_n-1) / (a-1);
}

size_t vehicle_is_crash_us(cJSON *node, const char *our_name, const char *our_color)
{
	cJSON *data;
	data = cJSON_GetObjectItem(node, "data");
	if (data){
		if (strcmp(cJSON_GetObjectItem(data, "name")->valuestring, our_name) == 0){
			return 1;
		}
		else{
			return 0;
		}
	}
	else {
		log_error("crash message has no data node\n");
		return 0;
	}
}

size_t vehicle_is_lap_finished_us(cJSON *node, const char *our_name, const char *our_color)
{
	cJSON *data, *car;
	data = cJSON_GetObjectItem(node, "data");
	if (data){
		car = cJSON_GetObjectItem(data, "car");
		if (car){
			if (strcmp(cJSON_GetObjectItem(car, "name")->valuestring, our_name) == 0){
				return 1;
			}
			else{
				return 0;
			}
		}
		else{
			log_error("lap finished message has no car node\n");
			return 0;
		}
	}
	else{
		log_error("lap finished message has no data node\n");
		return 0;
	}
}

size_t vehicle_parse(struct vehicle *this, cJSON *node)
{
	char *name = NULL, *color = NULL;
	double length = 0, width = 0, guide_flag_pos = 0;
	cJSON *id_node;
	cJSON *dimensions_node, *length_node, *width_node, *guide_node;
	size_t ret = 0;

	if (node == NULL) {
		log_error("vehicle_parse(this, node): node is NULL.\n");
		return 0;
	}

	id_node = cJSON_GetObjectItem(node, "id");
	if (!id_node) {
		log_error("node has no id node.\n");
		return 0;
	}

	parse_id_node(id_node, &name, &color);

	dimensions_node = cJSON_GetObjectItem(node, "dimensions");
	if (!dimensions_node) {
		log_error("node has no dimensions node.\n");
		return 0;
	}

	length_node = cJSON_GetObjectItem(dimensions_node, "length");
	if (length_node) {
		length = length_node->valuedouble;
	} else {
		log_error("dimensions node has no length node.\n");
	}

	width_node = cJSON_GetObjectItem(dimensions_node, "width");
	if (width_node) {
		width = width_node->valuedouble;
	} else {
		log_error("dimensions node has no width node.\n");
	}

	guide_node = cJSON_GetObjectItem(dimensions_node, "guideFlagPosition");
	if (guide_node) {
		guide_flag_pos = guide_node->valuedouble;
	} else {
		log_error("dimensions node has no guideFlagPosition node.\n");
	}

	ret = vehicle_construct(this, name, color, length, width, guide_flag_pos);
	free(name);
	free(color);
	return ret;
}

void vehicle_parse_turbo(cJSON *node, size_t * const turbo_dur, size_t * const turbo_factor)
{
	cJSON *data, *dur, *factor;

	data = cJSON_GetObjectItem(node, "data");
	if (data){
		dur = cJSON_GetObjectItem(data, "turboDurationTicks");
		factor = cJSON_GetObjectItem(data, "turboFactor");
		if (dur && factor){
			*turbo_dur = dur->valueint;
			*turbo_factor = factor->valueint;
		}
		else{
			log_error("turbo message has incomplete info\n");
		}
	} else {
		log_error("turbo message has no data node.\n");
	}
}

size_t vehicle_parse_update(struct vehicle *this, struct track *track, cJSON *node)
{
	cJSON *angle_node, *piece_position_node, *piece_index_node;
	cJSON *in_piece_distance_node;
	cJSON *lane_node, *start_lane_index_node, *end_lane_index_node;
	cJSON *lap_node;

	double angle = 0;
	size_t segment_index = 0;
	double segment_offset = 0;
	size_t start_lane_index = 0, end_lane_index = 0;
	int lap_count = 0;

	if (this == NULL) {
		log_error("vehicle_parse_update(this, node): this is NULL.\n");
		return 0;
	}

	if (node == NULL) {
		log_error("vehicle_parse_update(this, node): node is NULL.\n");
		return 0;
	}

	angle_node = cJSON_GetObjectItem(node, "angle");
	if (angle_node) {
		angle = M_PI * angle_node->valuedouble / 180;
	} else {
		log_error("data node has no angle node.\n");
	}

	piece_position_node = cJSON_GetObjectItem(node, "piecePosition");
	if (piece_position_node == NULL) {
		log_error("data node has no piecePosition node.\n");
		return 0;
	}

	piece_index_node = cJSON_GetObjectItem(piece_position_node, "pieceIndex");
	if (piece_index_node) {
		segment_index = piece_index_node->valueint;
	} else {
		log_error("piecePosition node has no pieceIndex node.\n");
	}

	in_piece_distance_node = cJSON_GetObjectItem(piece_position_node, "inPieceDistance");
	if (in_piece_distance_node) {
		segment_offset = in_piece_distance_node->valuedouble;
	} else {
		log_error("piecePosition node has no inPieceDistance node.\n");
	}

	lane_node = cJSON_GetObjectItem(piece_position_node, "lane");
	if (lane_node == NULL) {
		log_error("piecePosition node has no lane node.\n");
		return 0;
	}

	start_lane_index_node = cJSON_GetObjectItem(lane_node, "startLaneIndex");
	if (start_lane_index_node) {
		start_lane_index = start_lane_index_node->valueint;
	} else {
		log_error("lane node has no startLaneIndex node.\n");
	}

	end_lane_index_node = cJSON_GetObjectItem(lane_node, "endLaneIndex");
	if (end_lane_index_node) {
		end_lane_index = end_lane_index_node->valueint;
	} else {
		log_error("lane node has no endLaneIndex node.\n");
	}

	lap_node = cJSON_GetObjectItem(piece_position_node, "lap");
	if (lap_node) {
		lap_count = lap_node->valueint;
	} else {
		log_error("piecePosition node has no lap node.\n");
	}

	return vehicle_update(this, track, angle, segment_index, segment_offset,
	                      start_lane_index, end_lane_index, lap_count);
}

void vehicle_print(struct vehicle *this)
{
	log_print("struct vehicle {\n");
	log_print("  name: %s\n", this->name);
	log_print("  color: %s\n", this->color);
	log_print("  length: %f\n", this->length);
	log_print("  width: %f\n", this->width);
	log_print("  guideFlagPosition: %f\n", this->guide_flag_offset);
	log_print("  velocity: %f\n", this->velocity);
	log_print("  drift: %f\n", this->drift);
	log_print("  segment_index: %u\n", this->segment_index);
	log_print("  segment_offset: %f\n", this->segment_offset);
	log_print("  lane_index_start: %u\n", this->lane_index_start);
	log_print("  lane_index_end: %u\n", this->lane_index_end);
	log_print("  lap_count: %d\n", this->lap_count);
	log_print("}\n");
}

size_t vehicle_update(struct vehicle *this, struct track *track, double angle,
                      size_t segment_index, double segment_offset,
                      size_t start_lane_index, size_t end_lane_index,
                      int lap_count)
{
	double distance = 0.0;
	double last_velocity, last_drift, last_d_drift;

	if (!this)
		return 0;

	last_velocity = this->velocity;
	last_drift = this->drift;
	last_d_drift = this->d_drift;

	if (this->segment_index != segment_index) {
		distance = track_get_distance_between_segments_on_lane(track,
		                                                       this->segment_index,
		                                                       segment_index,
		                                                       this->lane_index_end);
		this->velocity = distance - this->segment_offset + segment_offset;

		if (this->lane_index_end != this->lane_index_start) {
			/**
			 * It seems that this->segment_offset calculates the
			 * distance the car has traveled. If we switch lanes
			 * it adds to distance traveled by 2.060275. So this
			 * means that our segment distance calculation fails
			 * by this amount, so add it.
			 */
			this->velocity += 2.060275;
		}
	} else {
		this->velocity = segment_offset - this->segment_offset;
	}

	this->velocity_mean = this->velocity_mean * 0.9 + this->velocity * 0.1;
	this->drift = angle;
	this->segment_index = segment_index;
	this->segment_offset = segment_offset;
	this->lane_index_start = start_lane_index;
	this->lane_index_end = end_lane_index;
	this->lap_count = lap_count;

	this->d_drift = this->drift - last_drift;
	this->d_d_drift = this->d_drift - last_d_drift;

	if (this->vel_data_count < 3) {
		this->velocity_data[this->vel_data_count] = last_velocity;
		this->throttle_data[this->vel_data_count] = this->throttle;
		log_print("Recorded %f %f at %d.\n", last_velocity, 
			this->throttle, this->vel_data_count);

		/* Don't accept zero velocities to avoid DIV 0.
		 *
		 * Additionally, if zero velocity is recorded, it means the
		 * measuring period got interrupted and should thus be 
		 * restarted. */
		this->vel_data_count = (this->vel_data_count + 1) * 
			(last_velocity > 0);
	} else if (this->vel_data_count == 3) {
		double v_0 = this->velocity_data[0];
		double v_1 = this->velocity_data[1];
		double v_2 = this->velocity_data[2];
		double T_0 = this->throttle_data[0];
		double T_1 = this->throttle_data[1];

		double b = (v_1*v_1 - v_2*v_0)/(T_0*v_1 - T_1*v_0);
		double a = (v_1 - b * T_0) / v_0;

		this->vel_const_a = a;
		this->vel_const_b = b;

		log_print("Velocity coefficients: a = %f, b = %f.\n", a, b);
		this->vel_data_count += 1; /* HACK to avoid further analysis.*/
	}

	return 1;
}

void vehicle_vector_destroy(struct vector *vehicles)
{
	unsigned int i;
	struct vehicle *vehicle;

	for (i=0; i<vehicles->elem_cnt; ++i) {
		struct vehicle *vehicles_array = vehicles->elem;
		vehicle = &vehicles_array[i];
		vehicle_destroy(vehicle);
	}

	vector_destroy(vehicles);
}

struct vehicle *vehicle_vector_find(struct vector *vehicles, char *name,
                                    char *color)
{
	unsigned int i;
	struct vehicle *vehicle = NULL;

	for (i=0; i<vehicles->elem_cnt; ++i) {
		struct vehicle *vehicles_array = vehicles->elem;
		vehicle = &vehicles_array[i];

		if (strcmp(vehicle->color, color) == 0 &&
		    strcmp(vehicle->name, name) == 0)
		{
			return vehicle;
		}
	}

	return NULL;
}

size_t vehicle_vector_init(struct vector *vehicles, cJSON *json)
{
	size_t len;
	size_t i;
	cJSON *data, *race, *cars, *car;
	struct vehicle v;
	if (!json){
		return 0;
	}
	data = cJSON_GetObjectItem(json, "data");
	if (!data){
		return 0;
	}
	race = cJSON_GetObjectItem(data, "race");
	if (!race){
		return 0;
	}
	cars = cJSON_GetObjectItem(race, "cars");
	if (!cars){
		return 0;
	}
	len = cJSON_GetArraySize(cars);
	vector_construct(vehicles, sizeof(struct vehicle), len);

	for (i = 0; i < len; ++i){
		car = cJSON_GetArrayItem(cars, i);
		vehicle_parse(&v, car);
		vector_push_back(vehicles, &v);
	}
	return 1;
}

void vehicle_vector_parse_update(struct vector *vehicles, struct track *track,
                                 cJSON *node)
{
	cJSON *data_node, *id_node, *tmp;
	char *name, *color;
	int i, data_size;

	if (node == NULL) {
		log_error("vehicle_vector_parse_update(vehices, node): node is NULL.\n");
		return;
	}

	data_node = cJSON_GetObjectItem(node, "data");
	if (data_node == NULL) {
		log_error("node has no data node.\n");
		return;
	}

	data_size = cJSON_GetArraySize(data_node);
	for (i=0; i<data_size; ++i) {
		tmp = cJSON_GetArrayItem(data_node, i);
		id_node = cJSON_GetObjectItem(tmp, "id");
		if (id_node == NULL) {
			log_error("data node has no id node.\n");
			continue;
		}

		name = NULL;
		color = NULL;
		parse_id_node(id_node, &name, &color);
		vehicle_parse_update(vehicle_vector_find(vehicles, name, color),
		                     track, tmp);
		free(name);
		free(color);
	}
}

void vehicle_vector_print(struct vector *vehicles)
{
	unsigned int i;
	struct vehicle *vehicle;
	log_print("vehicles: [\n");

	for (i=0; i<vehicles->elem_cnt; ++i) {
		struct vehicle *vehicles_array = vehicles->elem;
		vehicle = &vehicles_array[i];
		vehicle_print(vehicle);
	}

	log_print("]\n");
}
