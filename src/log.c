#include "log.h"

#include <errno.h>
#include <stdio.h>
#include <stdarg.h>

#define LOG_BUFSIZE 1024

void log_print(const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	vprintf(fmt, ap);
	va_end(ap);
}

void log_error(const char *fmt, ...)
{
	char buf[LOG_BUFSIZE];

	va_list ap;
	va_start(ap, fmt);
	vsnprintf(buf, LOG_BUFSIZE, fmt, ap);
	va_end(ap);

	if (errno)
		perror(buf);
	else
		fprintf(stderr, "%s", buf);
}
