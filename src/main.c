#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#include "ai.h"
#include "ai_switch_lane.h"
#include "ai_turbo.h"
#include "bot.h"
#include "log.h"
#include "track.h"
#include "vehicle.h"

static int connect_to(char *hostname, char *port)
{
	int status;
	int fd = -1;
	char portstr[32];
	struct addrinfo hint;
	struct addrinfo *info;

	srand(time(0));

	memset(&hint, 0, sizeof(struct addrinfo));
	hint.ai_family = PF_INET;
	hint.ai_socktype = SOCK_STREAM;

	snprintf(portstr, 32, "%d", atoi(port));

	status = getaddrinfo(hostname, portstr, &hint, &info);
	if (status != 0) {
		log_error("failed to get address: %s", gai_strerror(status));
		return -1;
	}

	fd = socket(PF_INET, SOCK_STREAM, 0);
	if (fd < 0) {
		log_error("failed to create socket");
		return -1;
	}

	status = connect(fd, info->ai_addr, info->ai_addrlen);
	if (status < 0) {
		log_error("failed to connect to server");
		return -1;
	}

	freeaddrinfo(info);
	return fd;
}


int main(int argc, char *argv[])
{
	int sock, done = 0;
	struct message recv_msg, send_msg;
	struct track track;
	struct vector vehicles;
	char *our_name = NULL, *our_color = NULL;
	char *track_name = NULL, *password = NULL;
	struct vehicle *our_vehicle = NULL;
	size_t car_count = 0;
	double throttle;
	struct switch_lane ai_switch_lanes;
	size_t has_turbo = 0, turbo_dur = 0, turbo_factor = 0;
	size_t ai_state = AI_STATE_WARMUP;

	send_msg.game_tick = -1;

	if (argc < 5 || argc > 8) {
		log_error("Usage: %s <bot> <host> <port> <botname> <botkey>\n",
		          argv[0]);
		return -1;
	}

	if (argc == 6){
		car_count = atoi(argv[5]);
	}
	if (argc == 7){
		track_name = argv[5];
		car_count = atoi(argv[6]);
	}
	if (argc == 8){
		track_name = argv[5];
		password = argv[6];
		car_count = atoi(argv[7]);
	}

	sock = connect_to(argv[1], argv[2]);
	if (sock < 0) {
		log_error("Socket is invalid\n");
		return -1;
	}

	ai_switch_lane_construct(&ai_switch_lanes);

	if (argc == 5) {
		message_create_join(&send_msg, argv[3], argv[4]);
	} else {
		message_create_adv_join(&send_msg, argv[3], argv[4],
		                        track_name, password, car_count);
	}

	message_print(&send_msg, "send");
	message_send(&send_msg, sock);
	message_free(&send_msg);

	while (!done) {
		message_read(&recv_msg, sock);
		message_print(&recv_msg, "recv");

		/* TODO: handle here different types of messages.
		 *
		 * e.g. we receive game init, here we can send the json to
		 * track handler, which can parse it the way it wants
		 */
		switch (recv_msg.type) {
		case BOT_MSG_TYPE_CAR_POSITIONS:
			vehicle_vector_parse_update(&vehicles, &track, recv_msg.json);

			ai_assess_lanes(&ai_switch_lanes, &track, &vehicles,
			                our_vehicle);

			if (ai_switch_lanes.state == AI_SWITCH_LANE_STATE_DECIDED &&
			    !ai_switch_lanes.sent)
			{
				ai_switch_lanes.sent = 1;
				message_create_switch_lane(&send_msg,
				                           &ai_switch_lanes);
			} else if (ai_assess_turbo(our_vehicle, &track,
			                           &vehicles)) {
				our_vehicle->has_turbo = 0; /* TODO: setter! */
				message_create_turbo(&send_msg);
			} else {
				throttle = ai_get_throttle(ai_state,
				                           &track, &vehicles, 
				                           our_vehicle);
				message_create_throttle(&send_msg, throttle);
			}


			/* vehicle_vector_print(&vehicles); */
			break;
		case BOT_MSG_TYPE_GAME_INIT:
			track_construct(&track, &recv_msg);
			track_print(&track);
			vehicle_vector_init(&vehicles, recv_msg.json);
			vehicle_vector_print(&vehicles);
			our_vehicle = vehicle_vector_find(&vehicles, our_name,
			                                  our_color);
			message_create_ping(&send_msg);
			break;
		case BOT_MSG_TYPE_YOUR_CAR:
			parse_your_car_node(&recv_msg, &our_name, &our_color);
			log_print("our name: %s, our color: %s\n", our_name,
			          our_color);
			message_create_ping(&send_msg);
			break;
		case BOT_MSG_TYPE_TOURNAMENT_END:
			done = 1;
			message_create_ping(&send_msg);
			break;
		case BOT_MSG_TYPE_TURBO_AVAIL:
			has_turbo = 1;
			vehicle_parse_turbo(recv_msg.json, &turbo_dur, &turbo_factor);
			vehicle_enable_turbo(our_vehicle, turbo_factor, turbo_dur);
			message_create_ping(&send_msg);
			break;
		case BOT_MSG_TYPE_CRASH:
			if (vehicle_is_crash_us(recv_msg.json, our_name, our_color)){
				printf("STATS: drift amount was %f\n", our_vehicle->drift);
			}
			message_create_ping(&send_msg);
			break;
		case BOT_MSG_TYPE_LAP_FINISHED:
			if (vehicle_is_lap_finished_us(recv_msg.json, our_name, our_color)){
				ai_state = AI_STATE_TOPSPEED;
			}
			message_create_ping(&send_msg);
			break;
		case BOT_ERROR:
			log_print("unknown message received:\n%s\n",
			          cJSON_PrintUnformatted(recv_msg.json));
		default:
			message_create_ping(&send_msg);
			break;
		}

		message_send(&send_msg, sock);
		message_print(&send_msg, "send");

		message_free(&send_msg);
		message_free(&recv_msg);
	}

	free(our_name);
	free(our_color);
	vehicle_vector_destroy(&vehicles);

	return 0;
}
