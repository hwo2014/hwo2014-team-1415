#ifndef TRACK_H
#define TRACK_H

#include "bot.h"
#include "vector.h"

struct track_segment;
struct track_lane;
struct vehicle;

/**
 * Track is a race track consisting of sequentially connected segments.
 */
struct track {
	char *id;
	char *name;

	struct vector lanes;
	struct vector segments; /** Typoed to discourage direct access. */
	struct vector segment_lookup; /** Maps partial to full segments. */
	struct vector speed_limits;
};

#ifdef __cplusplus
extern "C" { 
#endif

	/**
	 * Adds a new lane to the track.
	 *
	 * @param this	Pointer to the subject track structure.
	 * @param lane	Pointer to the track lane to be added.
	 */
	void track_add_lane(struct track *this, const struct track_lane *lane);

	/**
	 * Adds a new track segment to the end of the track.
	 *
	 * Inconsistencies (such as track overlap or disconnected start and 
	 * goal) are not checked/enforced.
	 *
	 * @param this	Pointer to the subject track structure.
	 * @param seg	Pointer to the track segment to be added.
	 */
	void track_add_segment(struct track *this,
	                       const struct track_segment *seg);

	/**
	 * Default constructor for the track structure.
	 *
	 * @param this	   Pointer to the subject track structure.
	 * @param message  game init message
	 */
	void track_construct(struct track *this, struct message *game_init);

	/**
	 * Default destructor for the track structure.
	 *
	 * @param this	Pointer to the subject track structure.
	 */
	void track_destroy(struct track *this);

	/**
	 * Count similar (e.g. straight) adjacent track segments.
	 *
	 * @param this	Pointer to the subject tract structure.
	 * @param idx	Index of first track segment to count.
	 * @return	The number of adjacent bends starting from idx.
	 */
	unsigned int track_get_adjacent_similar_segment_count(
		const struct track *this, unsigned int idx);

	/**
	 * Calculate the distance that would need to be traveled on given lane
	 * between two segments.
	 *
	 * @param this      Pointer to the subject tract structure.
	 * @param a         index of a segment
	 * @param b         index of b segment
	 * @param lane_idx  lane index.
	 */
	double track_get_distance_between_segments_on_lane(
		const struct track *this, int a, int b, unsigned int lane_idx);

	/**
	 * Calculate the distance to the next switch containing track segment.
	 *
	 * @param this  Pointer to the subject track structure.
	 * @param car   Pointer to the car whose distance to determine
	 * @return      distance to next switch, -1 if has no switches,
	 *                                        0 if on switch segment.
	 */
	double track_get_distance_to_switch(const struct track *this,
	                                    const struct vehicle *car);

	/**
	 * Calculate what would be the distance if the given vehicle would
	 * travel from first switch to second switch on a given lane.
	 *
	 * @param this      Pointer to the subject tract structure.
	 * @param car       car which to do the calculations on.
	 * @param lane_idx  Lane index
	 * @return          distance.
	 */
	double track_get_distance_between_switches(const struct track *this,
	                                           const struct vehicle *car,
	                                           unsigned int lane_idx);

	/**
	 * Calculate the current maximum speed for vehicle.
	 *
	 * @param this	Pointer to the subject track structure.
	 * @param car	Pointer to the car whose max speed to determine.
	 */
	double track_get_max_vehicle_speed(struct track *this,
	                                   const struct vehicle *car);

	/**
	 * Return number of switches the track has.
	 *
	 * @param this	Pointer to the subject track structure.
	 * @return      Number of switches in track.
	 */
	unsigned int track_get_number_of_switches(const struct track *this);

	/**
	 * Return the track segment corresponding to given index.
	 *
	 * @param this	Pointer to the subject tract structure.
	 * @param idx	Track segment's index.
	 * @return	The requested track segment or null on error.
	 */
	const struct track_segment *track_get_segment(const struct track *this,
	                                       unsigned int idx);

	/**
	 * Return the number of segments on this track
	 *
	 * @param this	Pointer to the subject tract structure.
	 * @return The number of track segments.
	 */
	unsigned int track_get_segment_count(const struct track *this);

	/**
	 * Return the true offset within requested segment.
	 *
	 * @param this	Pointer to the subject tract structure.
	 * @param idx	Track segment's index.
	 * @param ofst	Current offset within the segment.
	 */
	double track_get_segment_offset(const struct track *this,
	                                unsigned int idx, double ofst);

	/**
	 * Debug pretty print the track.
	 *
	 * @param this	Pointer to the subject track structure.
	 */
	void track_print(struct track *this);

#ifdef __cplusplus
}
#endif

#endif
