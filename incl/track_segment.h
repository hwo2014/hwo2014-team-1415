#ifndef TRACK_SEGMENT_H
#define TRACK_SEGMENT_H

#include <stddef.h>

#include "cJSON.h"

struct vehicle;

/**
 * Track segment is a piece of a race track.
 *
 * Race track consists of straight and bent segments laid after one another.
 * Bent track segments are always circle archs. All segments are flat.
 */
struct track_segment {
	double length; /** Length of segment's central lane. */
	double radiuz; /** Length of the sector's (this segment's) side. */
	double angle;  /** Angle of the sector that forms this segment. */
	size_t has_lane_switch;
};

#ifdef __cplusplus
extern "C" { 
#endif
	/**
	 * Construct track segment from JSON node.
	 *
	 * @param this	Pointer to the subject track segment structure.
	 * @param node  Pointer to the json node of this.
	 *
	 * @return      result of track_segment_construct
	 */
	size_t track_segment_parse(struct track_segment *this, cJSON *node);

	/**
	 * Default constructor for the track segment structure.
	 *
	 * If length > 0, radius and angle will be derived from it. If 
	 * radius > 0, length will be derived from it and angle. If both
	 * length and radius > 0, failure is returned.
	 *
	 * The angle determines to direction of the track segment as in the
	 * unit circle: negative angle is a turn right, positive a turn left.
	 *
	 * @param this	Pointer to the subject track segment structure.
	 * @param len	Length of the segment.
	 * @param r	Radius of the circle that'd be formed by this segment.
	 * @param a	Angle of the circular segment in radians.
	 * @param sw	Set iff lane switching is possible at the segment.
	 * @return	True iff track segment was initialized successfully.
	 */
	size_t track_segment_construct(struct track_segment *this, double len,
	                               double r, double a, size_t sw);

	/**
	 * Return the length of the desired lane at this track segment.
	 *
	 * @param this	Pointer to the subject track segment structure.
	 * @param entry	Distance of the entry lane from the central lane.
	 * @param exit	Distance of the exit lane from the central lane.
	 * @return	Return the length of the desired lane.
	 */
	double track_segment_get_lane_length(const struct track_segment *this,
	                                     double entry, double exit);

	/**
	 * Return the maximum velocity on a lane in this track segment.
	 *
	 * The angle determines the direction as in the unit circle:
	 * negative angle is to the right, positive to the left.
	 *
	 * @param this	Pointer to the subject track segment structure.
	 * @param vecl	The subject vehicle which races along the segment.
	 * @param cnt	Number of similar segmentsconencted to this segment.
	 * @param entry	Distance of the entry lane from the central lane.
	 * @param exit	Distance of the exit lane from the central lane.
	 * @return	Maximum velocity on the track segment per time unit.
	 */
	double track_segment_get_max_velocity(const struct track_segment *this,
	                                      const struct vehicle *vecl,
	                                      unsigned int cnt, double entry, 
	                                      double exit);

	/**
	 * Calculate the radius of desired lane on the track segment.
	 *
	 * @param this	Pointer to the subject track segment structure.
	 * @param lane	Distance of the target lane from the central lane.
	 * @return	Distance from the segments "origin" to desired lane
	 * @return	or 0.00 for straight segments.
	 */
	double track_segment_get_radius(const struct track_segment *this,
	                                double lane);

	/**
	 * Return the maximum velocity on a lane in this track segment.
	 *
	 * @param this	Pointer to the subject track segment structure.
	 * @param cnt	Number of similar segmentsconencted to this segment.
	 * @param offst Distance of the lane from the central lane.
	 * @return	Maximum velocity on the track segment per time unit.
	 */
	double track_segment_get_speed_limit(const struct track_segment *this,
                                      unsigned int cnt, double offst);

#ifdef __cplusplus
}
#endif

#endif
