#ifndef AI_H_
#define AI_H_

#define AI_STATE_WARMUP 0
#define AI_STATE_TOPSPEED 1

struct track;
struct vector;
struct vehicle;

#ifdef __cplusplus
extern "C" { 
#endif

/**
 * Function that calculates the throttle for the next throttle message.
 * Calculation is done according to the passed ai state.
 *
 * @param ai_state AI state to do the calculation with. Possible values
 * are listed in ai.h
 * @param track pointer to the track
 * @param vehicles pointer to vehicles vector
 * @param our_vehicle pointer to our vehicle
 *
 * @return amount of throttle
 */
double ai_get_throttle(const int ai_state, 
	struct track *track, 
	const struct vector *vehicles,
	struct vehicle *our_vehicle);

/**
 * Predictive throttle calculation.
 *
 * Looks n segments ahead, and adjust throttle by their angle and
 * radius.
 *
 * @param track        Pointer to track
 * @param our_vehicle  Pointer to our vehicle
 * @param n            How many segments to look ahead for.
 */
double ai_get_predictive_throttle(const struct track *track,
	                                  const struct vehicle *our_vehicle,
	                                  unsigned int n);

#ifdef __cplusplus
}
#endif

#endif
