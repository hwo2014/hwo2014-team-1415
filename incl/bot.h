#ifndef __BOT_H
#define __BOT_H

#include <sys/types.h>

#include "cJSON.h"

/*
 * Types of messages in the protocol
 * https://helloworldopen.com/techspec
 */

/** This will be put into MSG_TYPE in case of error */
#define BOT_ERROR                   0x00

/** Messages thar are created by the bot */
#define BOT_MSG_TYPE_JOIN           0x01
#define BOT_MSG_TYPE_THROTTLE       0x02
#define BOT_MSG_TYPE_PING           0x03
/** these are advanced use only */
#define BOT_MSG_TYPE_CREATE_RACE    0x04
#define BOT_MSG_TYPE_JOIN_RACE      0x05

/** Message that are sent by the server */
#define BOT_MSG_TYPE_YOUR_CAR       0x06
#define BOT_MSG_TYPE_GAME_INIT      0x07
#define BOT_MSG_TYPE_GAME_START     0x08
#define BOT_MSG_TYPE_CAR_POSITIONS  0x09
#define BOT_MSG_TYPE_GAME_END       0x0A
#define BOT_MSG_TYPE_TOURNAMENT_END 0x0B
#define BOT_MSG_TYPE_CRASH          0x0C
#define BOT_MSG_TYPE_SPAWN          0x0D
#define BOT_MSG_TYPE_LAP_FINISHED   0x0E
#define BOT_MSG_TYPE_DNF            0x0F
#define BOT_MSG_TYPE_FINISH         0x10
#define BOT_MSG_TYPE_SWITCH_LANE    0x11
#define BOT_MSG_TYPE_TURBO_ACTIVATE 0x12
#define BOT_MSG_TYPE_TURBO_AVAIL    0x13

struct switch_lane;

struct message
{
	unsigned int type; /** type of the message, defined ^ */
	int game_tick;     /** game tick of the message, if available */
	cJSON *json;       /** raw json data */
};

#ifdef __cplusplus
extern "C" {
#endif

	/**
	 * Debug print message.
	 *
	 * @param message  pointer to message
	 * @param type     type of message to identify the debug print.
	 */
	void message_print(struct message *this, const char *type);

	/**
	 * Convert the string msg type into integer.
	 *
	 * @param message type string
	 *
	 * @return message type integer
	 */
	unsigned int message_type(const char *str);

	/**
	 * Read message from the server into given Message structure.
	 *
	 * @param this  Pointer to Message
	 * @param fd    socket file description
	 */
	void message_read(struct message *this, int fd);

	/**
	 * Parse the JSON message setting the values into Message
	 * accordingly.
	 *
	 * @param this  Pointer to Message
	 * @param json  json object
	 */
	void message_parse(struct message *this, cJSON *json);

	/**
	 * Create a join message.
	 *
	 * @param this  pointer to message
	 * @param name  bot name
	 * @param key   bot key
	 */
	void message_create_join(struct message *this, const char *name,
	                         const char *key);

	/**
	 * Creates and advanced join message.
	 *
	 * @param this pointer to message
	 * @param name bot name
	 * @param key bot key
	 * @param track track name, can be NULL
	 * @param password race password, can be NULL
	 * @param car_count number of cars
	 */
	void message_create_adv_join(struct message *this, const char *name,
		const char *key, const char *track, const char *password, const size_t car_count);

	/**
	 * Create a ping message
	 *
	 * @param this  pointer to message
	 */
	void message_create_ping(struct message *this);

	/**
	 * Create a throttle message
	 * (For now without game tick)
	 *
	 * @param this      pointer to message
	 * @param throttle  current throttle, 0.0 to 1.0
	 */
	void message_create_throttle(struct message *this, double throttle);

	/**
	 * Create a turbo message
	 *
	 * @param this      pointer to message
	 */
	void message_create_turbo(struct message *this);

	/**
	 * Create a lane switch message.
	 *
	 * @param this        Pointer to message
	 * @param switch_lane Pointer to ai switch lane construct.
	 */
	void message_create_switch_lane(struct message *this,
	                                struct switch_lane *switch_lane);

	/**
	 * Free the memory allocated by the message.
	 *
	 * This should be called after having called message_create_*
	 *
	 * @param this  pointer to message.
	 */
	void message_free(struct message *this);

	/**
	 * Write the message to socket file descriptor aka. send to server.
	 *
	 * @param this  pointer to message
	 * @param fd    socket file descriptor
	 */
	void message_send(struct message *this, int fd);

#ifdef __cplusplus
}
#endif

#endif /* __BOT_H */
