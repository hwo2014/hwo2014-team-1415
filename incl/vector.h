#ifndef VECTOR_H
#define VECTOR_H

struct vector {
	void *elem;
	unsigned int elem_size;
	unsigned int elem_max;
	unsigned int elem_cnt;
};

#ifdef __cplusplus
extern "C" { 
#endif

	/**
	 * Reset the element counter.
	 * 
	 * No memory is freed and contents of vector are left untouched.
	 *
	 * @param this	Pointer to subject vector.
	 */
	void vector_clear(struct vector *this);

	/**
	 * Initialize vector structure.
	 *
	 * @param this	Pointer to subject vector.
	 * @param size	Size of a single element in bytes.
	 * @param cpty	Vector's initial capacity of elements.
	 */
	void vector_construct(struct vector *this, unsigned int size,
	                      unsigned int cpty);

	/**
	 * Free all memory allocated by the vector.
	 *
	 * @param this	Pointer to subject vector.
	 */
	void vector_destroy(struct vector *this);

	/**
	 * Add an element after the element at largest index.
	 * The source element's contents are copied "as is" into the vector.
	 *
	 * @param this	Pointer to subject vector.
	 * @param elem	Pointer to source element.
	 */
	void vector_push_back(struct vector *this, const void *elem);

	/**
	 * Remove the element at the largest index from the vector.
	 *
	 * @param this	Pointer to subject vector.
	 */
	void vector_pop_back(struct vector *this);

	/**
	 * Remove the element at index.
	 *
	 * The element at given index will be overwritten by the last
	 * element of the vector and the (original) last element is dropped.
	 * In other words, order of the elements will not be preserved.
	 *
	 * @param this	Pointer to subject vector.
	 * @param index	Index of element to be removed.
	 * @return	Number of elements dropped - 1 on success, 0 on error.
	 */
	unsigned int vector_remove(struct vector *this, unsigned int index);

#ifdef __cplusplus
}
#endif

#endif

