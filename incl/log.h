#ifndef __LOG_H
#define __LOG_H

/*
 * Generic logging/printing utilities.
 */

#ifdef __cplusplus
extern "C" {
#endif

	/**
	 * Print to stdout.
	 *
	 * @param  fmt format of the string
	 * @param  ... parameters
	 */
	void log_print(const char *fmt, ...);

	/**
	 * Print error to stdout with possibly using perror
	 *
	 * @param  fmt format of the string
	 * @param  ... parameters
	 */
	void log_error(const char *fmt, ...);

#ifdef __cplusplus
}
#endif

#endif /* __LOG_H */
