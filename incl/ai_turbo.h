#ifndef __AI_TURBO_H
#define __AI_TURBO_H

#include <stddef.h>

struct track;
struct vector;
struct vehicle;

#ifdef __cplusplus
extern "C" {
#endif

	/**
	 * Determine wether or not now is a goot time to use turbo.
	 *
	 * @param our_vehicle  Pointer to our vehicle
	 * @param track        Pointer to track
	 * @param vehicles     Pointer to vehicles vector
	 */
	size_t ai_assess_turbo(const struct vehicle *our_vehicle,
	                       const struct track *track,
	                       const struct vector *vehicles);

#ifdef __cplusplus
}
#endif

#endif
