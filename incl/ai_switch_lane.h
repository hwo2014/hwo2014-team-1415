#ifndef __AI_SWITCH_LANE_H
#define __AI_SWITCH_LANE_H

#define AI_SWITCH_LANE_NO    0x40
#define AI_SWITCH_LANE_LEFT  0x41
#define AI_SWITCH_LANE_RIGHT 0x42

/* State of switching lane */
#define AI_SWITCH_LANE_STATE_DECIDED   0x43
#define AI_SWITCH_LANE_STATE_UNDECIDED 0x44

/* This is how many pixels beforehand we decide the switch
 * As 'switch' is determined to be a segment of track that contains switch. */
#define AI_SWITCH_LANE_NOT_BEFORE 25

struct track;
struct vector;
struct vehicle;

struct switch_lane {
	int chosen_lane;      /* LEFT, RIGHT or NO, which lane to switch to. */
	int decision_segment_idx; /* index of the segment where decided */
	int state;  /* current state of the decision, DECIDED or UNDECIDED. */
	int sent;   /* boolean flag */
};

#ifdef __cplusplus
extern "C" {
#endif

	/**
	 * Default constructor for the ai switch lane construct.
	 *
	 * @param this  pointer
	 */
	void ai_switch_lane_construct(struct switch_lane *this);

	/**
	 * Assess the lanes.
	 *
	 * @param state        Pointer to state of the ai switch lanes.
	 * @param track        Pointer to track
	 * @param vehicles     Pointer to vehicles vector
	 * @param our_vehicle  Pointer to our vehicle
	 */
	void ai_assess_lanes(struct switch_lane *state, struct track *track,
	                     struct vector *vehicles, struct vehicle *our_vehicle);

#ifdef __cplusplus
}
#endif

#endif /* __AI_SWITCH_LANE_H */
