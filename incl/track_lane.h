#ifndef __TRACK_LANE_H
#define __TRACK_LANE_H

#include <stddef.h>
#include "cJSON.h"

/**
 * Track has a number of lanes, lanes are the strides the vehicles move on.
 */
struct track_lane {
	double distance; /** distance from the center of the track */
	int index;       /** Index of the lane */
};

#ifdef __cplusplus
extern "C" {
#endif
	/**
	 * Construct track lane from JSON node.
	 *
	 * @param this	Pointer to the subject track lane structure.
	 * @param node  Pointer to the json node of this.
	 */
	void track_lane_parse(struct track_lane *this, cJSON *node);

	/**
	 * Default constructor for the track lane structure.
	 *
	 * @param this	    Pointer to the subject track lane structure.
	 * @param distance  Distance from center of the track
	 * @param index     index of the lane, starts from zero.
	 */
	void track_lane_construct(struct track_lane *this, double distance,
	                          int index);

#ifdef __cplusplus
}
#endif

#endif /* __TRACK_LANE_H */
