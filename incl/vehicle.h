#ifndef VEHICLE_H
#define VEHICLE_H

#include "bot.h"
#include "cJSON.h"
#include "vector.h"

#include <stddef.h>

struct track;

/**
 * Vehicle is the thing racing on the track.
 */
struct vehicle {
	char *name;
	char *color;              /** Car color: ID within a track. */

	double velocity_data[3];  /** Measured velocities. */
	double throttle_data[3];  /** Measured throttle causing velocities. */
	size_t vel_data_count;    /** Number of velocity measurements made. */

	double centrif_force;     /** Maximum centrifugal force. */
	double vel_const_a;       /** Physical constant for velocity. */
	double vel_const_b;       /** Physical constant for velocity. */

	double length, width;     /** Length and width of vehicle. */
	double guide_flag_offset; /** Guide flag's offset (from front). */
	double throttle;     /** Last throttle value sent to server. */
	double velocity;     /** Current velocity (distances per tick). */
	double velocity_mean;     /** Average velocity for the vehicle. */
	double turbo_factor;      /** Turbo strength (velocity multiplier). */
	double turbo_duration;    /** Turbo duration in game ticks. */
	double drift_max;         /** Max drift before crashing. */

	double drift;            /** Current amount of drift. */
	double d_drift;          /** Last change in the amount of drift. */
	double d_d_drift;        /** Last change in the change of drift. */
	double segment_offset;   /** Distance traveled within segment. */
	size_t segment_index;    /** Segment where the guide flag is now. */
	size_t lane_index_start; /** Lane along which the segm. was entered. */
	size_t lane_index_end;   /** Lane along which the segm. is exited. */
	int    lap_count;
	int    has_turbo;        /** True iff turbo is available for use. */
};

#ifdef __cplusplus
extern "C" { 
#endif
	/**
	 * Function for parsing the id node.
	 *
	 * This function will malloc the memory for name & color, calling
	 * function must free.
	 *
	 * @param node   JSON containing id node.
	 * @param name   Pointer pointer to name.
	 * @param color  Pointer pointer to color.
	 */
	void parse_id_node(cJSON *node, char **name, char **color);

	/**
	 * Function for parsing the yourCar message.
	 *
	 * This function will malloc the memory for name & color, calling
	 * function must free.
	 *
	 * @param node   JSON containing id node.
	 * @param name   Pointer pointer to name.
	 * @param color  Pointer pointer to color.
	 */
	void parse_your_car_node(struct message *msg, char **name, char **color);

	/**
	 * Default constructor for the vehicle structure.
	 *
	 * @param this	          Pointer to the subjectvehicle structure.
	 * @param name	          Name of the vehicle.
	 * @param color	          Color of the vehicle.
	 * @param length          length of the vehicle.
	 * @param width           width of the vehicle.
	 * @param guide_flag_pos  position of the guide flag.
	 *
	 * @return	True iff the structure was initialized successfully.
	 */
	size_t vehicle_construct(struct vehicle *this, const char *name,
	                         const char *color, double length, double width,
	                         double guide_flag_pos);

	/**
	 * Default destructor for the vehicle structure.
	 *
	 * @param this	Pointer to the subject vehicle structure.
	 */
	void vehicle_destroy(struct vehicle *this);

	/**
	 * Tell the vehicle that it can now use turbo.
	 *
	 * @param this	Pointer to the subject vehicle structure.
	 * @param trbo	Speed multiplier the turbo gives.
	 * @param dur	Turbo duration in ticks.
	 */
	void vehicle_enable_turbo(struct vehicle *this, double trbo,
	                          double dur);

	/**
	 * Calculate the maximum initial velocity from which the vehicle is
	 * able to decelerate to desired final velocity before traveling
	 * further than given maximum distance.
	 *
	 * @param this	Pointer to the subject vehicle structure.
	 * @param vel	Final velocity that the vehicle must reach.
	 * @param len	Distance during which the final vel. must be reached.
	 * @return	Maximum initial velocity.
	 */
	double vehicle_get_initial_velocity(const struct vehicle *this,
	                                    double vel, double len);

	/**
	 * Calculate the number of ticks during which the vehicle travels the
	 * desired distance when velocity and throttle are constant.
	 * 
	 * @param this	Pointer to the subject vehicle structure.
	 * @param len	Distance to travel.
	 * @return	The max tick count before distance is exceeded.
	 */
	double vehicle_get_ticks_to_distance(const struct vehicle *this,
                                     double dist);

	/**
	 * Calculate the vehicle velocity after N ticks at constant throttle.
	 *
	 * @param this	Pointer to the subject vehicle structure.
	 * @param n	Number of ticks after which the velocity is measured.
	 * @param thrl	Throttle that is maintained during the next n ticks.
	 */
	double vehicle_get_velocity_at_tick(const struct vehicle *this,
	                                    size_t n, double thrl);

	/**
	 * Function that checks if we crashed.
	 *
	 * @param node crash message json
	 * @param our_name our name
	 * @parma our_color our color
	 *
	 * @return true if we crashed
	 */
	size_t vehicle_is_crash_us(cJSON *node, const char *our_name, const char *our_color);

	size_t vehicle_is_lap_finished_us(cJSON *node, const char *our_name, const char *our_color);

	/**
	 * Parse vehicle structure from the JSON
	 *
	 * @param this  Pointer to the subject vehicle structure.
	 * @param node  JSON node that points to the this vehicle.
	 * @return      result of vehicle_construct(this) with parsed values.
	 */
	size_t vehicle_parse(struct vehicle *this, cJSON *node);

	/**
	 * Function for parsing available turbo. Only used with our own car.
	 *
	 * @param node turboAvailable message json
	 * @param turbo_dur parse target of turbo duration
	 * @param turbo_factor parse target of turbo factor
	 */
	void vehicle_parse_turbo(cJSON *node, size_t * const turbo_dur, size_t * const turbo_factor);

	/**
	 * Update the vehicle from passed carPositions message.
	 *
	 * @param this   Pointer to the subject vehicle structure.
	 * @param track  Pointer to track
	 * @param json   JSON node that points to the this vehicle update.
	 * @return       FALSE if update failed
	 */
	size_t vehicle_parse_update(struct vehicle *this, struct track *track,
	                            cJSON *node);

	/**
	 * Pretty print the vehicle to stdout
	 *
	 * @param this  Pointer to the subject vehicle structure.
	 */
	void vehicle_print(struct vehicle *this);

	/**
	 * Update routine for the vehicle structure.
	 *
	 * @param this	           Pointer to the subject vehicle structure.
	 * @param track            Pointer to track
	 * @param angle
	 * @param segment_index
	 * @param segment_offset
	 * @param start_lane_index
	 * @param end_lane_index
	 * @param lap_count
	 *
	 * @return FALSE(0) on error, != 0 on success.
	 */
	size_t vehicle_update(struct vehicle *this, struct track *track,
	                      double angle, size_t segment_index,
	                      double segment_offset, size_t start_lane_index,
	                      size_t end_lane_index, int lap_count);

	/**
	 * Default destructor for the vehicles vector structure.
	 *
	 * @param vehicles  Pointer to the vehicles vector structure
	 */
	void vehicle_vector_destroy(struct vector *vehicles);

	/**
	 * Find the given vehicle from the vehicle vector
	 *
	 * @param vehicles  Pointer to vehicle vector to find vehicles from
	 * @param name      Name of the vehicle to find
	 * @param color     color of the vehicle to find
	 *
	 * @return pointer to vehicle
	 */
	struct vehicle *vehicle_vector_find(struct vector *vehicles, char *name,
	                                    char *color);

	/**
	 * Initialize vehicle vector from passed gameInit message
	 *
	 * @param vehicles	target vector to initialize
	 * @param json		gameInit message json
	 *
	 * @return 		false if something goes wrong
	 */
	size_t vehicle_vector_init(struct vector *vehicles, cJSON *json);

	/**
	 * Update the vehicles vector from passed carPositions message.
	 *
	 * @param vehicles  target vector to update
	 * @param track     Pointer to track.
	 * @param json      carPositions message json.
	 */
	void vehicle_vector_parse_update(struct vector *vehicles,
	                                 struct track *track, cJSON *node);

	/**
	 * Pretty print the vehicles vector to stdout
	 *
	 * @param vehicles  vehicles vector to print
	 */
	void vehicle_vector_print(struct vector *vehicles);

#ifdef __cplusplus
}
#endif

#endif
