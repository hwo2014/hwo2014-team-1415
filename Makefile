BIN=bin/
INCL=incl/
LIB=lib/
SRC=src/
TEST=test/

CC=gcc
FLAGS=-pedantic -Wall -Wextra -O2 -lm # -ggdb
# Use GNU find instead of Make's wildcard.
SRCS=$(shell find $(SRC) -iname '*.c' -not -iname "main.c")
OBJS=$(SRCS:%.c=%.o) # Make each source file into a target.

.PHONY: clean

# http://stackoverflow.com/questions/3087459/how-to-define-rules-in- \
# the-makefile-to-compile-only-that-cpp-files-which-was-m

# Make the object files prerequisites of the executable.
# Thus program is rebuilt whenever one of the SRCS changes.
all: main

# Make the header files prerequisites of the objects.
#$(OBJS): %.o : %.h

.c.o:
	$(CC) $(FLAGS) -I $(INCL) -c $*.c -o $(BIN)$(@F)

main: $(OBJS)
	$(CC) $(FLAGS) -I $(INCL) $(SRC)/main.c $(BIN)/*o -o $(BIN)/bot.elf
	$(CC) $(FLAGS) -I $(INCL) $(TEST)/main.c $(BIN)/*o -o $(BIN)/test.elf

clean:
	rm $(BIN)/*

