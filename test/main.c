#include "bot.h"
#include "log.h"
#include "track.h"
#include "vehicle.h"

#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "cJSON.h"

int test_parse_game_init(const char *path)
{
	struct message msg;
	struct track track;
	struct vector vehicles;
	int fd = open(path, O_RDONLY);
	if (!fd) {
		log_error("unable to open file: %s\n", path);
		return 1;
	}

	message_read(&msg, fd);
	message_print(&msg, "test");

	track_construct(&track, &msg);
	track_print(&track);
	track_destroy(&track);

	vehicle_vector_init(&vehicles, msg.json);
	vehicle_vector_print(&vehicles);
	vehicle_vector_destroy(&vehicles);

	message_free(&msg);

	return 0;
}

int test_parse_car_positions(const char *init, const char *positions)
{
	struct message msg;
	struct track track;
	struct vector vehicles;
	int fd_init, fd_pos;

	fd_init = open(init, O_RDONLY);
	if (!fd_init) {
		log_error("unable to open file: %s\n", init);
		return 1;
	}

	fd_pos = open(positions, O_RDONLY);
	if (!fd_pos) {
		log_error("unable to open file: %s\n", positions);
		return 1;
	}

	message_read(&msg, fd_init);
	track_construct(&track, &msg);
	vehicle_vector_init(&vehicles, msg.json);
	log_print("INIT:\n");
	vehicle_vector_print(&vehicles);
	message_free(&msg);

	close(fd_init);

	message_read(&msg, fd_pos);
	vehicle_vector_parse_update(&vehicles, &track, msg.json);
	log_print("\n\nUPDATE:\n");
	vehicle_vector_print(&vehicles);
	message_free(&msg);

	close(fd_pos);

	track_destroy(&track);
	vehicle_vector_destroy(&vehicles);

	return 0;
}

int test_parse_turbo_available(const char *path)
{
	struct message msg;
	size_t turbo_dur = 0, turbo_factor = 0;
	int fd = open(path, O_RDONLY);
	if (!fd) {
		log_error("unable to open file: %s\n", path);
		return 1;
	}

	message_read(&msg, fd);
	message_print(&msg, "test");

	vehicle_parse_turbo(msg.json, &turbo_dur, &turbo_factor);

	if (turbo_dur != 0 && turbo_factor != 0){
		return 0;
	}
	else{
		return 1;
	}
}

int test_track_speed_limit_calculation()
{
	return 0;
}

/*
 * Simple test framework.
 * Parameters should define the test case.
 *
 * @return 0 == OK, something else means error.
 */
int main(int argc, char **argv)
{
	if (argc == 1) {
		log_print("Usage: %s <test case> [test args]\n", argv[0]);
		return -1;
	}

	if (strcmp(argv[1], "gameInit") == 0) {
		if (argc != 3) {
			log_error("FAILURE: Missing sample json file..\n");
			return -1;
		}

		return test_parse_game_init(argv[2]);
	} else if (strcmp(argv[1], "carPositions") == 0) {
		if (argc != 4) {
			log_error("FAILURE: Missing sample json files for "
				"game init and car position.\n");
			return -1;
		}

		return test_parse_car_positions(argv[2], argv[3]);
	} else if (strcmp(argv[1], "speedLimit") == 0) {
		return test_track_speed_limit_calculation();
	} else if (strcmp(argv[1], "turbo") == 0) {
		if (argc != 3){
			log_error("FAILURE: Missing sample json file..\n");
			return -1;
		}

		return test_parse_turbo_available(argv[2]);
	} else {
		log_error("Unknown test case '%s'. "
			"Available test cases are\n\tgameInit\n"
			"\tcarPositions\n\tspeedLimit\n\tturbo.\n", argv[1]);
		return -1;
	}
}
